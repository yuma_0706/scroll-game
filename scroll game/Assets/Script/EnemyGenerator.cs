﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public float span = 1.0f;
    float delta = 0;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;

        Debug.Log(delta);
        if (delta > 0.5f)
        {
            delta = 0;
            GameObject go = Instantiate(EnemyPrefab);
            int py = Random.Range(-7, 8);
            go.transform.position = new Vector3(6, py, 0);
        }
    }

    
}
