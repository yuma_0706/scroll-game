﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.1f, 0, 0);//同じ速さで落下

        if(transform.position.x < -10.0f)
        {
            Destroy(gameObject);
                 
        }

        //当たり判定
        Vector2 p1 = transform.position;
        Vector2 p2 = player.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;//敵の半径
        float r2 = 0.5f;//パックマンの半径

        if (d < r1 + r2)
        {
           Destroy(gameObject);//衝突したら矢を消す
        }

    }
}
